= README

Simple, small and straight forward image. This docker image does not need any python or javascript.

This image contains only a few shell scripts to bascially run the official speedtest cli.

This image does not contains any Grafana or InfluxDB but it will connect to your InfluxDB. 

This image use cron tasks so you can customize extensively how often you run your tests without reinventing the wheel of scheduling tasks.

== Performance

I first found the project of barrycarey (https://github.com/barrycarey/Speedtest-for-InfluxDB-and-Grafana). Unfortunately, the project does not seem to be maintained and was far from reporting realistic values as we can see below.

.Previous image
image::doc/old.png[]

.New (this repo) image
image::doc/new.png[]

== ENV variables

- `INFLUXDB_HOST`: You *must* provide this one
- `INFLUXDB_PORT`: You may provide a custom port if you don't use the standard 8086
- `SERVER`: You can leave it empty to use any server of provide an ID (see logs at start to see the ids)
- `CRON`: The default is `0 * * * *` which effectively runs every hour. You can get crazier using https://crontab.guru/
- `DB`: The default is `speedtests` and the database will be created for you. If you prefer another, you may use this variable 

== Run docker container

.The basic (that will not work)
    docker run --rm -it --network host chevdor/speedtest

NOTE: This will never work because the default value for `INFLUXDB_HOST` is set to `localhost`. That would mean that the influxdb server runs within the container and it definitely does not.

NOTE: Depdending on your default config, you may benefit from setting the network mode explicitely with `--network host` as shown above.

.Define your influxdb
You will need to at least define the InfluxDB Host IP:

    docker run --rm -it --network host -e INFLUXDB_HOST=192.168.0.123 chevdor/speedtest

.If you need to play with the cron task to change the frequency of the tests for instance
By default, the cron job will run every hour at minute 0 (see https://crontab.guru/every-1-hour).

For your first tests, you may want to run the script every minute:

    docker run --rm -it --network host -e INFLUXDB_HOST=192.168.0.123 -e CRON="* * * * *" chevdor/speedtest

== Build image

    docker build -t chevdor/speedtest .

== Sample output

The logs show you what is going on, which makes it easier to debug (and we see a little bug actually :)
----
include::doc/sample-output.txt[]
----