#!/usr/bin/env bash

source /etc/profile
DB=${DB:-speedtests}
INFLUXDB_HOST=${INFLUXDB_HOST:-127.0.0.1}
INFLUXDB_PORT=${INFLUXDB_PORT:-8086}

echo "The data will be sent to $INFLUXDB_HOST:$INFLUXDB_PORT in database $DB..."

if [ -z "$SERVER" ]
then
      FILTER=""
      echo Using any server
else
      FILTER="-s $SERVER"
      echo Using server id:$SERVER
fi

echo Running speedtest. It takes a few seconds, please wait...
JSON=`speedtest $SPEEDTEST_ARGS --accept-license --accept-gdpr $FILTER -f json`


# Here we escape the spaces and remove the double quotes
function escape {
    RES=$@
    RES=`echo ${RES} | sed 's/, /\-/g'`
    RES=`echo ${RES} | sed 's/ /\\ /g'`
    printf "%q" "$RES"
}

# echo Parsing data...
DOWNLOAD_RAW=`echo $JSON | jq -r .download.bandwidth`
DOWNLOAD=`bc <<<"scale=2; $DOWNLOAD_RAW / 125000"`

UPLOAD_RAW=`echo $JSON | jq -r .upload.bandwidth`
UPLOAD=`bc <<<"scale=2; $UPLOAD_RAW / 125000"`

PING=`echo $JSON | jq -r .ping.latency`
JITTER=`echo $JSON | jq -r .ping.jitter`
SERVER=`echo $JSON | jq -r .server.id`

ISP=`echo $JSON | jq -r .isp`
ISP=`escape $ISP`

SERVER_NAME=`echo $JSON | jq -r .server.name` 
SERVER_NAME=`escape $SERVER_NAME`

SERVER_COUNTRY=`echo $JSON | jq -r .server.country`
SERVER_COUNTRY=`escape $SERVER_COUNTRY`

SERVER_LOCATION=`echo $JSON | jq -r .server.location`
SERVER_LOCATION=`escape $SERVER_LOCATION`

# echo "▼$DOWNLOAD_RAW ▲$UPLOAD_RAW"
echo "$SERVER: $SERVER_COUNTRY/$SERVER_LOCATION/$SERVER_NAME   ▼$DOWNLOAD Mbps  ▲$UPLOAD Mbps  ◉$PING ms"

PAYLOAD="speed_test_results,server=${SERVER},server_name=${SERVER_NAME},server_country=${SERVER_COUNTRY},server_location=${SERVER_LOCATION},isp=$ISP upload=${UPLOAD},download=${DOWNLOAD},ping=$PING,jitter=$JITTER"
# printf "PAYLOAD: %q\n" "$PAYLOAD"

echo Sending data to influxdb 
curl -S -s -XPOST "http://$INFLUXDB_HOST:$INFLUXDB_PORT/write?db=$DB" --data-binary "$PAYLOAD"    

if [ $? -eq 0 ]; then
    echo Done, successfully
else
    echo FAIL
fi
